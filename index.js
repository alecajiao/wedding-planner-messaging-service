import {Datastore} from '@google-cloud/datastore';
import express from 'express';
import cors from 'cors';
import axios from 'axios';

const PORT = process.env.PORT || 3005;
const app = express();
app.use(express.json());
app.use(cors());

const datastore = new Datastore();

// GET /messages
// GET /messages/:mid
// GET /messages?recipient=someone
// GET /messages?sender=someone
// GET /messages?sender=someone&recipient=someoneelse
// Should return messages where both conditions are met

// POST /messages
// Request Body {"sender": "bill@wed.com", "recipient": "jane@wed.com", "note":"great job"}
// The service should add a timestamp property to the message when you create a message
// The service should make a request to the Authorization Service to verify both emails are valid before saving the information.

app.post('/messages', async (req, res) => {
    const message = req.body;
    let senderStatus = null;
    let recipientStatus = null;

    try {
        const response  = await axios.get(`http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users/${message.sender}/verify`);
        console.log(response.status);
        senderStatus = response.status;
    } catch (error) {
        console.log("Sender not found");
        senderStatus = 404;
    }

    try {
        const response  = await axios.get(`http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users/${message.recipient}/verify`);
        console.log(response.status);
        recipientStatus = response.status;

    } catch (error) {
        console.log("Recipient not found");
        recipientStatus = 404;
    }

    if(senderStatus === 200 & recipientStatus === 200){
        message.created = new Date();
        const key = datastore.key("Message");
        const response = await datastore.save({key: key, data: message})
        console.log(response);
        res.status(200).send("Message was successfully sent.");
    }else {
        res.status(404).send("Unable to verify accounts");
    }
;
})

// GET /messages
// GET /messages?recipient=someone
// GET /messages?sender=someone
// GET /messages?sender=someone&recipient=someoneelse
app.get('/messages', async (req,res) => {
    const sender = req.query.sender;
    const recipient = req.query.recipient;

    console.log(recipient);
    console.log(sender);

    if(sender && recipient){
        const query = datastore
        .createQuery('Message')
        .filter('recipient', '=', recipient)
        .filter('sender', '=', sender);

        console.log(query);

        const [messages, metaInfo] = await datastore.runQuery(query);
        console.log(messages);
        console.log(metaInfo);
        res.status(200).send(messages);

    }else if (sender){
        const query = datastore
        .createQuery('Message')
        .filter('sender', '=', sender);

        const [messages, metaInfo] = await datastore.runQuery(query);
        console.log(messages);
        console.log(metaInfo);
        res.status(200).send(messages);

    }else if(recipient) {
        const query = datastore
        .createQuery('Message')
        .filter('recipient', '=', recipient);

        const [messages, metaInfo] = await datastore.runQuery(query);
        console.log(messages);
        console.log(metaInfo);
        res.status(200).send(messages);

    }else{
        const query  =  datastore.createQuery('Message');
        const messages = (await datastore.runQuery(query))[0];
        console.log(messages);
        res.status(200).send(messages);
    }
});

// GET /messages/:mid
app.get('/messages/:mid', async (req, res) => {
    console.log(req.params.mid);
    const key = datastore.key(["Message", Number(req.params.mid)]);
    const response = await datastore.get(key);

    console.log(response);
    res.status(200).send(response);
});

app.listen(PORT, ()=>{console.log(`Server started on port ${PORT}`)});