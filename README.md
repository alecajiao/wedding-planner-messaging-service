# Wedding Planner Messaging Service

## POST 
### `https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages`
### Format
`{
    "sender": "misty@email.com",
    "recipient": "dani@email.com",
    "note": "Project is almost finished."
}`

## GET
### `https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages`

## GET messages?recipient=
### `https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?recipient=dani@email.com`

## GET /messages?sender=
### `https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?sender=sam@email.com`

## GET messages?sender=&recipient=
### `https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?sender=dani@email.com&recipient=misty@email.com`

## GET by ID
### `https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages/:mid`

